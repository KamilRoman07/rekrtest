﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Rekr_test
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            string result = Reverse("Kamil Roman");
            int[,] matrixToProcess = new int[,] { { 0,1,2,3,4},{ 1,1,2,3,4}, {2,2,2,3,4 },{ 3,3,3,3,4},{ 4,4,4,4,4} };
            int[,] resultMatrix = ProcessMatrix(matrixToProcess);
        }

        private int[,] ProcessMatrix(int[,] matrixToProcess)
        {
            int[,] resultMatrix = new int[,] { {0,0},{0,0} };
            int rowAmount = matrixToProcess.GetUpperBound(0) - matrixToProcess.GetLowerBound(0) + 1;
            int colAmount = matrixToProcess.GetUpperBound(1) - matrixToProcess.GetLowerBound(1) + 1;
            for (int i = 0; i < rowAmount; i++)
            {
                for (int j = 0; j < rowAmount; j++)
                {
                    switch (CheckPosition(i, j, rowAmount))
                    {
                        case 1:
                            resultMatrix[0,0] += matrixToProcess[i,j];
                            break;
                        case 2:
                            resultMatrix[0,1] += matrixToProcess[i,j];
                            break;
                        case 3:
                            resultMatrix[1,0] += matrixToProcess[i,j];
                            break;
                        case 4:
                            resultMatrix[1,1] += matrixToProcess[i,j];
                            break;
                        default:
                            break;
                    }
                }
            }
            return resultMatrix;
        }

        private int CheckPosition(int i, int j, int rowAmount)
        {
            if (i == j || (i + j) == rowAmount-1)
                return 0;
            else if (i < j && i+j < rowAmount-1)
                return 1;
            else if (i < j && i+j > rowAmount-1)
                return 2;
            else if (i > j && i+j < rowAmount-1)
                return 4;
            else
                return 3;
        }

        private static string Reverse(string toReverse)
        {
            string result = "";
            if (!char.IsDigit(toReverse[toReverse.Length - 1]))
                result += char.ToUpper(toReverse[toReverse.Length - 1]);
            else
                result += toReverse[toReverse.Length - 1];
            for (int i = toReverse.Length-2; i >= 0; i--)
            {
                if (!char.IsDigit(toReverse[i]))
                    result += char.ToLower(toReverse[i]);
                else
                    result += toReverse[i];
            }
            return result;
        }
    }
}
